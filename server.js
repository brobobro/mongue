const express        = require('express');
const MongoClient    = require('mongodb').MongoClient;
const bodyParser     = require('body-parser');
const app            = express();
const morgan         = require('morgan');
const chalk          = require('chalk');

const DB_CONFIG      = require('./config/db');
const Routes         = require('./routes');
const PORT = 8181;

const morganMiddleware = morgan(function (tokens, req, res) {


  return [
      '\n',
      chalk.hex('#ff4757').bold('🍄  Morgan --> '),
      chalk.hex('#34ace0')(tokens.method(req, res)),
      chalk.hex('#ffb142')(tokens.status(req, res)),
      chalk.hex('#ff5252')(tokens.url(req, res)),
      chalk.hex('#2ed573')(tokens['response-time'](req, res) + ' ms'),
      chalk.hex('#f78fb3')('@ ' + tokens.date(req, res)),
      chalk.yellow(tokens['remote-addr'](req, res)),
      // chalk.hex('#fffa65')('from ' + tokens.referrer(req, res)),
      chalk.hex('#1e90ff')(tokens['user-agent'](req, res)),
      '\n',
  ].join(' ');
});

app.use(morganMiddleware)

app.use(bodyParser.urlencoded({ extended: true, useNewUrlParser: true }));

async  function connect () {
  await MongoClient.connect(DB_CONFIG.url, (err, database) => {
    if (err) return console.log(err)
    console.log('Connected.')
    Routes(app, database);
    app.listen(PORT, () => {
      console.log('We are live on ' + PORT);
    });
  })
}

connect()

