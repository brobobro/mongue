import moment from 'moment';
moment.locale('ru');

const makeDaysInMonth = (options) => {

  let daysInMonth =moment().subtract(4, 'months').daysInMonth() //moment().subtract('1', 'months')['daysInMonth']();
  let arrDays = [];

  while(daysInMonth) {
    let day = moment().date(daysInMonth).format('DD');
    let weekDay = moment().date(daysInMonth).format('dddd');
    let month = moment().date(daysInMonth).format('MMMM');
    let current = { day, weekDay, month };
    arrDays.push(current);
    daysInMonth--;
  }

  console.log(arrDays);

  return arrDays;
}

export default makeDaysInMonth
